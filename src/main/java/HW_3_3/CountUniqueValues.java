package HW_3_3;

import java.util.*;
import java.util.stream.Collectors;

public class CountUniqueValues {

    String str;
    CountUniqueValues(String str){
        this.str = str.toLowerCase();

    }

    public static void main(String[] args) {

        String str = "Как у тебя дела?";
        CountUniqueValues countUniqueValues = new CountUniqueValues(str);

        for(Map.Entry<Character, Integer> entry : countUniqueValues.countUnique()) {
            Character key = entry.getKey();
            Integer value = entry.getValue();

            if(key == ' '){
                System.out.println("Пробелы: "+value);
                continue;
            }
            System.out.println(key+" "+value);
        }

    }

    /**
     * Метод для подсчета уникальных символом в строке
     * 1. Создается Map countUnique в которую буду положены уникальные значения,
     * ключ - символ строки, значение - частота символа
     * 2. C помощью условия в цикле проверется, нахождения уже имеющегося ключа. Если ключ присутвуется, то
     * к его значение прибавляется 1
     * 3. Для вывода частоты символов в порядке убывания, используется Stream sorted который имеет в качестве
     * аргумента компаратор reverseOrder(). С помощью collect преобразуем поток в List
     * @return List с отсортированными значеними частоты символов
     */
    public List<Map.Entry<Character, Integer>> countUnique(){

        TreeMap<Character, Integer> countUnique = new TreeMap<>();

        for (int i = 0; i < str.length(); i++) {
            if(countUnique.containsKey(str.charAt(i))){
                countUnique.put(str.charAt(i), countUnique.get(str.charAt(i))+1);
                continue;
            }
            countUnique.put(str.charAt(i), 1);
        }

        List<Map.Entry<Character, Integer>> sortCountUnique = countUnique.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue((Comparator.reverseOrder())))
                .collect(Collectors.toList());

        return sortCountUnique;
    }

}
